ivaconsole
==========

console docker image based on cocoon/ivatools


composition
========
debian:wheezy
	cocoon/python
		cocoon/ivatools
			ivafull_tools


setup
=====

on docker host (with git and fig installed)

```
cd /home/user/shared/projects
git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/ivafull.git
cd ivafull
fig up
``


use
===

jenkins:

open http://$dockerhost:8089 in a browser


console:

./console.sh



