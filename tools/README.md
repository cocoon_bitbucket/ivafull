tools
=====

iva full tools

image: ivafull_tools_1


composition
========
debian:wheezy
	cocoon/python
		cocoon/ivatools
			+ adb


use
===

```
docker -d -rm --volumes-from ivafull_data_1 ivafull_tools_1
```
where ivafull_data_1 is a data container supplying the /jenkins and /tests volumes